from scipy.stats import normaltest
from scipy.stats import ttest_ind
from scipy.stats.kde import gaussian_kde
from numpy import linspace
import json
# dummy data
from numpy.random import randn
import matplotlib.pyplot as plt

dummy0 = randn(100)
dummy1 = randn(100) + 1

# test for normality
print(normaltest(dummy0))
print(normaltest(dummy1))

# initialize kernels
kde0 = gaussian_kde(dummy0)
kde1 = gaussian_kde(dummy1)

kde0_values = kde0(
    linspace(
        min(dummy0.min(), dummy1.min()), (max(dummy0.max(), dummy1.max()))))
kde1_values = kde1(
    linspace(
        min(dummy0.min(), dummy1.min()), (max(dummy0.max(), dummy1.max()))))

kde_diffs0 = kde1_values - kde0_values
kde_diffs1 = kde0_values - kde1_values

print(ttest_ind(kde_diffs0, kde_diffs1))

# actual data
with open('narcissus.json') as narcissus_file:
    data = json.load(narcissus_file)

fig, ax = plt.subplots(2, 2)
ax[0, 0].plot(data['mm'])
ax[0, 1].plot(data['fm'])
ax[1, 0].plot(data['mr'])
ax[1, 1].plot(data['fr'])
ax[0, 0].set_title('Male Movie Recommendations')
ax[0, 1].set_title('Female Movie Recommendations')
ax[1, 0].set_title('Male Research Recommendations')
ax[1, 1].set_title('Female Research Recommendations')
plt.tight_layout()
plt.show()
