import tweepy
import json
import re
import gender_guesser.detector as gender

# user credentials for twitter API
ACCESS_TOKEN = '3302010911-HNvgzN5UXYlLiuwuAyDfRH3PsPFE7vWvRJxe0i7'
ACCESS_TOKEN_SECRET = 'W8ukWW0IwQaip1jnnN4kjYAosL3BUFtZm1rtZw7S0uY1P'

CONSUMER_KEY = 'G60zQhN3W2GRqciHuc3SN6IOP'
CONSUMER_SECRET = 'jtkWs0lpi3jPpkw2zarSxqRn0jMZ1DHe27mtQVni0XS4tYwugp'

# authenticate
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
api = tweepy.API(auth, wait_on_rate_limit=True)

# parameters and queries
research_query = 'looking for research recommendations'
movie_query = 'looking for movie recommendations'
# likely much smaller due to subsequent filtering
max_tweets = 100000
max_replies = 1000
# case insensitive
narcissus_words = ['I', 'me', 'mine', 'we', 'us', 'our', 'ours']

# initialize gender detector
detector = gender.Detector()

# get original tweets (no retweets)
research_tweets = [
    tweet for tweet in tweepy.Cursor(
        api.search, tweet_mode='extended', q=research_query).items(max_tweets)
    if not tweet.retweeted and 'RT @' not in tweet.full_text
]

movie_tweets = [
    tweet for tweet in tweepy.Cursor(
        api.search, tweet_mode='extended', q=movie_query).items(max_tweets)
    if not tweet.retweeted and 'RT @' not in tweet.full_text
]


# get replies to tweets
def get_replies(tweets, max_replies, timeout=999999):
    replies = []
    for tweet in tweets:
        print('Tweet:', tweet.full_text)
        for reply in tweepy.Cursor(
                api.search,
                q='to:' + tweet.author.screen_name,
                tweet_mode='extended',
                timeout=timeout).items(max_replies):
            if hasattr(tweet, 'in_reply_to_status_id_str'):
                if (reply.in_reply_to_status_id_str == tweet.id_str):
                    print('Reply:', reply.full_text)
                    replies.append(reply)
    return replies


research_tweet_replies = get_replies(research_tweets, max_replies)

movie_tweet_replies = get_replies(movie_tweets, max_replies)


# per-tweet gender checker
def get_user_gender(tweet):
    first_name = tweet.author.name.split()[0]
    return detector.get_gender(first_name)


male_research_tweet_replies = [
    tweet for tweet in research_tweet_replies
    if get_user_gender(tweet) == 'male'
]
female_research_tweet_replies = [
    tweet for tweet in research_tweet_replies
    if get_user_gender(tweet) == 'female'
]

male_movie_tweet_replies = [
    tweet for tweet in movie_tweet_replies if get_user_gender(tweet) == 'male'
]
female_movie_tweet_replies = [
    tweet for tweet in movie_tweet_replies
    if get_user_gender(tweet) == 'female'
]


def generate_narcissus_count_re(tweets, narcissus_words=narcissus_words):
    narcissus_count_list = []
    pattern = '(' + '|'.join(narcissus_words) + ')'
    for tweet in tweets:
        narcissus_count = len(re.findall(pattern, tweet.full_text))
        narcissus_count_list.append(narcissus_count)
    print('Narcissus count list:', narcissus_count_list)
    return narcissus_count_list


male_research_tweet_replies_narcissus = generate_narcissus_count_re(
    male_research_tweet_replies)
female_research_tweet_replies_narcissus = generate_narcissus_count_re(
    female_research_tweet_replies)

male_movie_tweet_replies_narcissus = generate_narcissus_count_re(
    male_movie_tweet_replies)
female_movie_tweet_replies_narcissus = generate_narcissus_count_re(
    female_movie_tweet_replies)


def get_tweet_lists_json(tweet_lists):
    json_tweet_lists = []
    for tweet_list in tweet_lists:
        json_tweet_lists.append([tweet._json for tweet in tweet_list])
    return json_tweet_lists


all_tweets_json = get_tweet_lists_json([
    research_tweets, movie_tweets, male_research_tweet_replies,
    female_research_tweet_replies, male_movie_tweet_replies,
    female_movie_tweet_replies
])

# write data
with open('data.json', 'w') as data_file:
    json.dump(all_tweets_json, data_file)

# write narcissus_count_lists
with open('narcissus.json', 'w') as narcissus_file:

    json.dump(
        dict(
            mr=male_research_tweet_replies_narcissus,
            fr=female_research_tweet_replies_narcissus,
            mm=male_movie_tweet_replies_narcissus,
            fm=female_movie_tweet_replies_narcissus), narcissus_file)
